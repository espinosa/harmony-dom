Extracted and forked Apache Harmony / Android DOM implementation of W3C XML DOM
===============================================================================

It is intended as an alternative to Java internal and Xerces implementations of W3C.org XML Document Object Model (DOM).  
Unique feature: It preserves original attribute order. This is the only real change to the original code.

Original source
https://android.googlesource.com/platform/libcore/+/master/luni/src/main/java/org/apache/harmony/xml/dom/
Sources extracted on 10/10/2016.

[Apache Harmony](https://harmony.apache.org/) project ([wikipedia link](https://en.wikipedia.org/wiki/Apache_Harmony)) is now retired but Google uses and amintains many of its sources as part of its [Android project](https://en.wikipedia.org/wiki/Apache_Harmony#Use_in_Android_SDK).

# Why Harmony DOM:

 * __It preserves original attribute order__ and this original order is kept in the DOM tree in memory structure. 
 This is useful when document is parsed and then serialized again, it reduces the number of changes in the _updated_ document.
 If the original order is not kept user gets most likely very high number of false positives when comparing updated with original document.
 * Simple starightforward implementation of DOM interfaces
 * Good starting point for more optimized implementations; ie hash maps to speed up XPath searches 
 * It can be used with any W3C.org standard technologies - parser, serializer, XPath, XSLT transformation, JAXP, JAXB, ..
 * It is stable, mature project originally developed as part of wide reaching Apache Harmony project, now actively maintained by Android project where this is a standard implementation
 * It may be seen as yet another XML library, but does not cause infamous XML hell, far from it, by following strictly _coding to interfaces_ it makes it less likely to happen. 

Why not:

 * Xerces library is very mature, well tested, timem proven. That why it was adopted by Java as its internal implementation.
 * Xerces DOM implementation may be faster when used with Xerces parser; same applies for internal Java implementation. Their parser checks specific implementation and  
   It uses something called _deferred load_ though I am not sure how and when this effectively helps. 
   
## Preserving original attribute order

W3C.org specification states onlt that _order of attribute specifications in a start-tag or empty-element tag is not significant_[1] but intuitive expectation from a basic implementation is that original order should be kept. Unfortunately internal Java and Xerces XML implementations for some obscured reason decided to use alphabetical order; and even that is not officialy guaranteed. Keeping properties order is very useful when document is parsed and then serialized again, it reduces the number of changes. That is very useful for tracking source file changes in systems like Git, or for serialized document assertions in unit tests. Re-serialized document should be exactly same as original if no changes were actively made there[2].  
   
Keeping order of XML Element attributes is certainly highly requested featute:

 * http://stackoverflow.com/questions/726395/order-of-xml-attributes-after-dom-processing
 * http://stackoverflow.com/questions/17725948/java-xml-library-that-preserves-attribute-order
 * http://stackoverflow.com/questions/31570962/read-write-xml-file-preserving-original-attribute-order

[1]  
https://www.w3.org/TR/2008/REC-xml-20081126/#sec-starttags

[2]  
Re-serialized document should be exactly same as original if no changes were actively made there. Well, there are complications.
DOM by nature does not store whitespace information from between attributes. Because of that parsers does not collect such information. To keep complete re-serialization fidelity would require a special parser, serializer and DOM extension. It is usually not a big problem, because space is usually one space character. Also source management systems like Git, or tools like `diff` or `patch` can effectively ignore whitespace changes.

# How to use

This project is just standalone DOM implementation, there is no parser or serializer, but you can use any library or framework supporting W3C interfaces.

Hosted on Bintray: https://bintray.com/espinosa/repo/harmony-dom/  

Maven configuration:

	<dependency>
		<groupId>org.bitbucket.espinosa</groupId>
		<artifactId>harmony-dom</artifactId>
		<version>1.0.0</version>
	</dependency>


## How to use with standard Java JDK XML library

### How to use with standard Java parser

	import java.io.InputStream;
	import javax.xml.parsers.DocumentBuilder;
	import javax.xml.parsers.DocumentBuilderFactory;
	import org.w3c.dom.Document;
	
	public static final String DOCUMENT_CLASS_NAME = "http://apache.org/xml/properties/dom/document-class-name";

	public Document parse(InputStream xmlStream) {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		docFactory.setAttribute(DOCUMENT_CLASS_NAME, org.apache.harmony.xml.dom.DocumentImpl.class.getName());
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(xmlStream);
	}

See: org.bitbucket.espinosa.hdom.util.XmlParserSerializer.ParserToHarmonyDom.parse(InputStream)

### How to use with standard Java serializer (Transformer based)

	import java.io.OutputStream;
	import javax.xml.transform.Transformer;
	import javax.xml.transform.TransformerFactory;
	import javax.xml.transform.dom.DOMSource;
	import javax.xml.transform.stream.StreamResult;

	public void serialize(Document document, OutputStream outputStream) {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(document);
		StreamResult result = new StreamResult(outputStream);
		transformer.transform(source, result);
	}

See org.bitbucket.espinosa.hdom.util.XmlParserSerializer.TransformerSerializer

### How to use with standard Java serializer (LSSerializer based)

	import org.w3c.dom.bootstrap.DOMImplementationRegistry;
	import org.w3c.dom.ls.DOMImplementationLS;
	import org.w3c.dom.ls.LSOutput;
	import org.w3c.dom.ls.LSSerializer;

	public void serialize(Document document, OutputStream outputStream) {
		DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
		DOMImplementationLS domImplementation = (DOMImplementationLS) registry.getDOMImplementation("LS");
		LSSerializer serializer = domImplementation.createLSSerializer();
		LSOutput output = domImplementation.createLSOutput();
		output.setEncoding(encoding);
		output.setByteStream(outputStream);
		serializer.write(document, output);
	}

See: org.bitbucket.espinosa.hdom.util.XmlParserSerializer.LsOutputSerializer


