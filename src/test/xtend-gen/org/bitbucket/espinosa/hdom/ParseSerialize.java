package org.bitbucket.espinosa.hdom;

import org.bitbucket.espinosa.hdom.util.TestXmlParserSerializer;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Extension;
import org.junit.Assert;
import org.junit.Test;

/**
 * Parse XML string and then parse it again, result must be exactly the same, no changes in whitespace or attributes order.
 * 
 * Basic tests, ignore XML namespaces.
 * 
 * TODO: automatic preserving of XML header
 * 
 * @author Espinosa
 */
@SuppressWarnings("all")
public class ParseSerialize {
  @Extension
  private TestXmlParserSerializer _testXmlParserSerializer = new TestXmlParserSerializer();
  
  @Test
  public void complexTest() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    _builder.newLine();
    _builder.append("<manifest:manifest xmlns:manifest=\"urn:oasis:names:tc:opendocument:xmlns:manifest:1.0\" manifest:version=\"1.2\">");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("<manifest:file-entry manifest:full-path=\"/\" manifest:version=\"1.2\" manifest:media-type=\"application/vnd.oasis.opendocument.text\"/>");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("<manifest:file-entry manifest:full-path=\"Thumbnails/thumbnail.png\" manifest:media-type=\"image/png\"/>");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("<manifest:file-entry manifest:full-path=\"styles.xml\" manifest:media-type=\"text/xml\"/>");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("<manifest:file-entry manifest:full-path=\"content.xml\" manifest:media-type=\"text/xml\"/>");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("<manifest:file-entry manifest:full-path=\"meta.xml\" manifest:media-type=\"text/xml\"/>");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("<manifest:file-entry manifest:full-path=\"settings.xml\" manifest:media-type=\"text/xml\"/>");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("<manifest:file-entry manifest:full-path=\"manifest.rdf\" manifest:media-type=\"application/rdf+xml\"/>");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("<manifest:file-entry manifest:full-path=\"Configurations2/accelerator/current.xml\" manifest:media-type=\"\"/>");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("<manifest:file-entry manifest:full-path=\"Configurations2/\" manifest:media-type=\"application/vnd.sun.xml.ui.configuration\"/>");
    _builder.newLine();
    _builder.append("</manifest:manifest>");
    final String complexXml = _builder.toString();
    String _string = complexXml.toString();
    TestXmlParserSerializer _parse = this._testXmlParserSerializer.parse(complexXml);
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    TestXmlParserSerializer _withHeader = _parse.withHeader(_builder_1.toString());
    String _serialize = _withHeader.serialize();
    Assert.assertEquals(_string, _serialize);
  }
  
  @Test
  public void simpleTest() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<aaa>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<bbb z=\"1\" a=\"2\">");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("</bbb>");
    _builder.newLine();
    _builder.append("</aaa>");
    final String xml = _builder.toString();
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append("<aaa>");
    _builder_1.newLine();
    _builder_1.append("\t");
    _builder_1.append("<bbb z=\"1\" a=\"2\">");
    _builder_1.newLine();
    _builder_1.append("\t");
    _builder_1.append("</bbb>");
    _builder_1.newLine();
    _builder_1.append("</aaa>");
    String _string = _builder_1.toString();
    TestXmlParserSerializer _parse = this._testXmlParserSerializer.parse(xml);
    String _serialize = _parse.serialize();
    Assert.assertEquals(_string, _serialize);
  }
}
