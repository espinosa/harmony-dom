package org.bitbucket.espinosa.hdom

import org.bitbucket.espinosa.hdom.util.TestXmlParserSerializer
import org.bitbucket.espinosa.hdom.util.XmlParserSerializer.TransformerSerializer
import org.bitbucket.espinosa.hdom.util.XmlParserSerializer.VanillaParser
import org.junit.Ignore
import org.junit.Test

import static org.junit.Assert.*

/**
 * Parse XML string and then parse it again, result must be exactly the same, no changes in
 * whitespace or attributes order.
 * 
 * Compare to stock Oracle Java / OpenJDK / Xerces DOM implementation.
 * In this case most of this tests FAIL. 
 * To enable CI I had to make them ignored. 
 * 
 * It does not matter what serializer implelemntation is used. Order of attributed is 
 * determined by DOM internal implementation and the stock Oracle Java seems to prefer
 * re-ordering attributes based on the their name. W3C.org does not specify
 * order of attributes, true, but this is a poor choice in any respect. 
 * 
 * @author Espinosa
 */
class ParseSerializeVanilla {

	extension TestXmlParserSerializer = new TestXmlParserSerializer(new VanillaParser(true),
		new TransformerSerializer())

	/**
	 * Test fails because serialized XML has different order of attributes
	 */
	@Test
	@Ignore
	def testWithAttributesInDescendingAplhanumricOrder() {
		val xml = '''
		<aaa>
			<bbb z="1" a="2">
			</bbb>
		</aaa>'''
		assertEquals(
			'''
			<aaa>
				<bbb z="1" a="2">
				</bbb>
			</aaa>'''.toString,
			xml.parse().serialize()
		)
	}

	/**
	 * This test pass even with stock DOM, but it is a brittle test still, as
	 * there is no guarantee about argument order
	 */
	@Test
	def testWithAttributesInAscendingAplhanumricOrder() {
		val xml = '''
		<aaa>
			<bbb aaa="1" bbb="2">
			</bbb>
		</aaa>'''
		assertEquals(
			'''
			<aaa>
				<bbb aaa="1" bbb="2">
				</bbb>
			</aaa>'''.toString,
			xml.parse().serialize()
		)
	}

	/**
	 * Test fails because serialized XML has different order of attributes
	 */
	@Test
	@Ignore
	def simpleTest() {
		val xml = '''
		<aaa b="111" a="222"/>'''
		assertEquals(
			'''
			<aaa b="111" a="222"/>'''.toString,
			xml.parse().serialize()
		)
	}

	/**
	 * Test fails because serialized XML has different order of attributes (namespaced version)
	 */
	@Test
	@Ignore
	def complexTest() {
		val complexXml = '''
		<?xml version="1.0" encoding="UTF-8"?>
		<manifest:manifest xmlns:manifest="urn:oasis:names:tc:opendocument:xmlns:manifest:1.0" manifest:version="1.2">
		 <manifest:file-entry manifest:full-path="/" manifest:version="1.2" manifest:media-type="application/vnd.oasis.opendocument.text"/>
		 <manifest:file-entry manifest:full-path="Thumbnails/thumbnail.png" manifest:media-type="image/png"/>
		 <manifest:file-entry manifest:full-path="styles.xml" manifest:media-type="text/xml"/>
		 <manifest:file-entry manifest:full-path="content.xml" manifest:media-type="text/xml"/>
		 <manifest:file-entry manifest:full-path="meta.xml" manifest:media-type="text/xml"/>
		 <manifest:file-entry manifest:full-path="settings.xml" manifest:media-type="text/xml"/>
		 <manifest:file-entry manifest:full-path="manifest.rdf" manifest:media-type="application/rdf+xml"/>
		 <manifest:file-entry manifest:full-path="Configurations2/accelerator/current.xml" manifest:media-type=""/>
		 <manifest:file-entry manifest:full-path="Configurations2/" manifest:media-type="application/vnd.sun.xml.ui.configuration"/>
		</manifest:manifest>'''
		assertEquals(
			complexXml.toString,
			complexXml.parse().withHeader('''<?xml version="1.0" encoding="UTF-8"?>''').serialize()
		)
	}
}
