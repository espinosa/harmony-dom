package org.bitbucket.espinosa.hdom

import org.bitbucket.espinosa.hdom.util.TestXmlParserSerializer
import org.bitbucket.espinosa.hdom.util.XmlParserSerializer.LsOutputSerializer
import org.bitbucket.espinosa.hdom.util.XmlParserSerializer.ParserToHarmonyDom
import org.junit.Test

import static org.junit.Assert.*

/**
 * Parse XML string and then parse it again, result must be exactly the same, no changes in whitespace or attributes order.  
 * 
 * Use LSSerializer based implementation.
 * Unfortunatelly it is not possible to set new line delimiter to LSSerializer, it always use unix newline, ignoring system settings,
 * so results have to be clean up before comparison.
 * 
 * @author Espinosa
 */
class ParseSerializeWithLsSerializer {

	extension TestXmlParserSerializer = new TestXmlParserSerializer(new ParserToHarmonyDom(false), new LsOutputSerializer())

	@Test
	def simpleTest() {
		val xml = '''
		<aaa>
			<bbb z="1" a="2">
			</bbb>
		</aaa>'''
		assertEquals(
			'''
			<aaa>
				<bbb z="1" a="2">
				</bbb>
			</aaa>'''.toString.replace("\r\n", "\n"),
			xml.parse().serialize()
		)
	}
	
	@Test
	def complexTest() {
		val complexXml = '''
		<?xml version="1.0" encoding="UTF-8"?>
		<manifest:manifest xmlns:manifest="urn:oasis:names:tc:opendocument:xmlns:manifest:1.0" manifest:version="1.2">
		 <manifest:file-entry manifest:full-path="/" manifest:version="1.2" manifest:media-type="application/vnd.oasis.opendocument.text"/>
		 <manifest:file-entry manifest:full-path="Thumbnails/thumbnail.png" manifest:media-type="image/png"/>
		 <manifest:file-entry manifest:full-path="styles.xml" manifest:media-type="text/xml"/>
		 <manifest:file-entry manifest:full-path="content.xml" manifest:media-type="text/xml"/>
		 <manifest:file-entry manifest:full-path="meta.xml" manifest:media-type="text/xml"/>
		 <manifest:file-entry manifest:full-path="settings.xml" manifest:media-type="text/xml"/>
		 <manifest:file-entry manifest:full-path="manifest.rdf" manifest:media-type="application/rdf+xml"/>
		 <manifest:file-entry manifest:full-path="Configurations2/accelerator/current.xml" manifest:media-type=""/>
		 <manifest:file-entry manifest:full-path="Configurations2/" manifest:media-type="application/vnd.sun.xml.ui.configuration"/>
		</manifest:manifest>'''
		assertEquals(
			complexXml.toString.replace("\r\n", "\n"),
			complexXml.parse().withHeader('''<?xml version="1.0" encoding="UTF-8"?>''').serialize().replace("\r\n", "\n")
		)
	}
}
