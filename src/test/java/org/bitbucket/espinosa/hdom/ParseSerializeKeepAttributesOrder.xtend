package org.bitbucket.espinosa.hdom

import org.bitbucket.espinosa.hdom.util.TestXmlParserSerializer
import org.junit.Test

import static org.junit.Assert.*

/**
 * Parse XML string and then parse it again, result must be exactly the same, no changes in whitespace or attributes order.
 * 
 * Focus on checking on keeping attribute order. 
 * Element attributes must follow defined order, not alphabetical order; their order must stay same, 
 * not be redefined by serialization.
 * 
 * @author Espinosa
 */
class ParseSerializeKeepAttributesOrder {

	extension TestXmlParserSerializer = new TestXmlParserSerializer()

	@Test
	def testWithAttributesInDescendingAplhanumricOrder() {
		val xml = '''
		<aaa>
			<bbb z="1" a="2">
			</bbb>
		</aaa>'''
		assertEquals(
			'''
			<aaa>
				<bbb z="1" a="2">
				</bbb>
			</aaa>'''.toString,
			xml.parse().serialize()
		)
	}

	@Test
	def testWithAttributesInAscendingAplhanumricOrder() {
		val xml = '''
		<aaa>
			<bbb aaa="1" bbb="2">
			</bbb>
		</aaa>'''
		assertEquals(
			'''
			<aaa>
				<bbb aaa="1" bbb="2">
				</bbb>
			</aaa>'''.toString,
			xml.parse().serialize()
		)
	}

	@Test
	def simpleTest() {
		val xml = '''
		<aaa b="111" a="222"/>'''
		assertEquals(
			'''
			<aaa b="111" a="222"/>'''.toString,
			xml.parse().serialize()
		)
	}

	@Test
	def complexTest() {
		val complexXml = '''
		<?xml version="1.0" encoding="UTF-8"?>
		<manifest:manifest xmlns:manifest="urn:oasis:names:tc:opendocument:xmlns:manifest:1.0" manifest:version="1.2">
		 <manifest:file-entry manifest:full-path="/" manifest:version="1.2" manifest:media-type="application/vnd.oasis.opendocument.text"/>
		 <manifest:file-entry manifest:full-path="Thumbnails/thumbnail.png" manifest:media-type="image/png"/>
		 <manifest:file-entry manifest:full-path="styles.xml" manifest:media-type="text/xml"/>
		 <manifest:file-entry manifest:full-path="content.xml" manifest:media-type="text/xml"/>
		 <manifest:file-entry manifest:full-path="meta.xml" manifest:media-type="text/xml"/>
		 <manifest:file-entry manifest:full-path="settings.xml" manifest:media-type="text/xml"/>
		 <manifest:file-entry manifest:full-path="manifest.rdf" manifest:media-type="application/rdf+xml"/>
		 <manifest:file-entry manifest:full-path="Configurations2/accelerator/current.xml" manifest:media-type=""/>
		 <manifest:file-entry manifest:full-path="Configurations2/" manifest:media-type="application/vnd.sun.xml.ui.configuration"/>
		</manifest:manifest>'''
		assertEquals(
			complexXml.toString,
			complexXml.parse().withHeader('''<?xml version="1.0" encoding="UTF-8"?>''').serialize()
		)
	}
}
