package org.bitbucket.espinosa.hdom.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.SAXException;

/**
 * Convenient utility to parse XML to DOM, forcing this Harmony DOM
 * implementation, and to serialize it back to text.
 * 
 * This class documents how to set custom DOM implementation to stock Java
 * {@link DocumentBuilderFactory}.
 * 
 * It provides two common implementations of serializer - {@link Transformer}
 * (JAXP) and newer based on {@link LSSerializer}
 * 
 * @author Espinosa
 */
public class XmlParserSerializer {
	private final InputParser parser;
	private final OutputSerializer serializer;


	public XmlParserSerializer() {
		this(new ParserToHarmonyDom(false), new TransformerSerializer());
	}

	public XmlParserSerializer(InputParser parser, OutputSerializer serializer) {
		this.parser = parser;
		this.serializer = serializer;
	}

	public interface InputParser {
		Document parse(InputStream xmlStream);
	}

	/**
	 * Parse XML using JVM default parser (DocumentBuilderFactory).
	 * 
	 * @param xmlStream
	 * @param namespaces
	 *            optional support for namespaces
	 * @return parsed document, or new document if stream was null
	 */
	public static class ParserToHarmonyDom implements InputParser {
		public static final String DOCUMENT_CLASS_NAME = "http://apache.org/xml/properties/dom/document-class-name";
		private final boolean namespaces;

		public ParserToHarmonyDom(boolean namespaces) {
			this.namespaces = namespaces;
		}

		public Document parse(InputStream xmlStream) {
			try {
				DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				docFactory.setAttribute(DOCUMENT_CLASS_NAME, org.apache.harmony.xml.dom.DocumentImpl.class.getName());
				if (namespaces) {
					docFactory.setNamespaceAware(true);
				}
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				Document doc;
				if (xmlStream != null) {
					doc = docBuilder.parse(xmlStream);
				} else {
					doc = docBuilder.newDocument();
				}
				return doc;
			} catch (SAXException | IOException | ParserConfigurationException e) {
				throw new ParseException(e);
			}
		}
	}

	/**
	 * Parse XML using JVM default parser (DocumentBuilderFactory).
	 * 
	 * @param xmlStream
	 * @param namespaces
	 *            optional support for namespaces
	 * @return parsed document, or new document if stream was null
	 */
	public static class VanillaParser implements InputParser {
		private final boolean namespaces;

		public VanillaParser(boolean namespaces) {
			this.namespaces = namespaces;
		}

		public Document parse(InputStream xmlStream) {
			try {
				DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				if (namespaces) {
					docFactory.setNamespaceAware(true);
				}
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				Document doc;
				if (xmlStream != null) {
					doc = docBuilder.parse(xmlStream);
				} else {
					doc = docBuilder.newDocument();
				}
				return doc;
			} catch (SAXException | IOException | ParserConfigurationException e) {
				throw new ParseException(e);
			}
		}
	}
	/**
	 * Serialize document back to textual form
	 * 
	 * @param document
	 * @param inputStream
	 * @return DOM
	 */
	public Document parse(InputStream inputStream) {
		return parser.parse(inputStream);
	}
	
	/**
	 * Serialize document back to textual form
	 * 
	 * @param document
	 * @param outputStream
	 */
	public void serialize(Document document, OutputStream outputStream) {
		serializer.serialize(document, outputStream);
	}

	/**
	 * Return result as String. Ideal for testing and smaller documents.
	 */
	public Document parseString(String xmlContent) {
		InputStream outputStream = null; 
		if (xmlContent != null && !xmlContent.isEmpty()) {
			outputStream =  new ByteArrayInputStream(xmlContent.getBytes(StandardCharsets.UTF_8));
		}
		return parse(outputStream);
	}

	/**
	 * Return result as String. Ideal for testing and smaller documents.
	 */
	public String serializeToString(Document document) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		serialize(document, out);
		return new String(out.toByteArray(), StandardCharsets.UTF_8);
	}

	/**
	 * Recent Java provide several serializer implelemntations, this is an
	 * interface to wrap them and potentially test them all
	 */
	public interface OutputSerializer {
		void serialize(Document document, OutputStream outputStream);
	}

	/**
	 * Serializer using {@link LSSerializer} to serialize DOM.
	 * 
	 * Oracle/OpenJDK/Xerces LSSerializer implementation is rather
	 * disappointing. It completely ignores requested new line delimiter
	 * {@link LSSerializer#setNewLine(String)} and always uses unix newline.
	 * Pretty printer has serious limitations, no way how to set intendation,
	 * both size and spaces or tabs.
	 */
	public static class LsOutputSerializer implements OutputSerializer {
		private final LSSerializer serializer; 
		private final DOMImplementationLS domImplementation;
		private final String encoding;

		public static final LsOutputSerializer SERIALIZER = new LsOutputSerializer();

		public LsOutputSerializer() {
			this("UTF-8", false);
		}

		public LsOutputSerializer(String encoding, boolean xmlDeclaration) {
			this.encoding = encoding;
			try {
				DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
				domImplementation = (DOMImplementationLS) registry.getDOMImplementation("LS");
				serializer = domImplementation.createLSSerializer();
				serializer.getDomConfig().setParameter("xml-declaration", xmlDeclaration); //  see http://stackoverflow.com/a/19701727/1185845
				serializer.setNewLine(null);  
				// http://stackoverflow.com/a/7618801/1185845
				// still does not work in Java 8 (10/10/2016)
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | ClassCastException e) {
				throw new SerializeException(e);
			}
		}

		public void serialize(Document document, OutputStream outputStream) {
			LSOutput output = domImplementation.createLSOutput();
			output.setEncoding(encoding);
			output.setByteStream(outputStream);
			serializer.write(document, output);
		}
	}

	/**
	 * Serializer using {@link javax.xml.transform.Transformer} to serialize DOM.
	 */
	public static class TransformerSerializer implements OutputSerializer {

		public static final TransformerSerializer SERIALIZER = new TransformerSerializer();

		public void serialize(Document document, OutputStream outputStream) {
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer;
			try {
				transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(document);
				StreamResult result = new StreamResult(outputStream);
				transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
				transformer.transform(source, result);
			} catch (TransformerConfigurationException e) {
				throw new SerializeException(e);
			} catch (TransformerException e) {
				throw new SerializeException(e);
			}
		}
	}

	public static class ParseException extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public ParseException(String message, Throwable cause) {
			super(message, cause);
		}

		public ParseException(String message) {
			super(message);
		}

		public ParseException(Throwable cause) {
			super(cause);
		}
	}

	public static class SerializeException extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public SerializeException(String message, Throwable cause) {
			super(message, cause);
		}

		public SerializeException(String message) {
			super(message);
		}

		public SerializeException(Throwable cause) {
			super(cause);
		}
	}
}
