package org.bitbucket.espinosa.hdom.util;

import org.bitbucket.espinosa.hdom.util.XmlParserSerializer.InputParser;
import org.bitbucket.espinosa.hdom.util.XmlParserSerializer.OutputSerializer;
import org.w3c.dom.Document;

/**
 * Convenient wrapper of {@link XmlParserSerializer} for using in string based
 * unit tests
 * 
 * @author Espinosa
 */
public class TestXmlParserSerializer {
	private Document document;
	private final XmlParserSerializer xps;
	private String xmlHeader;
	
	public TestXmlParserSerializer() {
		xps = new XmlParserSerializer();
	}
	
	public TestXmlParserSerializer(InputParser parserImpl, OutputSerializer outputSerializerImpl) {
		xps = new XmlParserSerializer(parserImpl, outputSerializerImpl);
	}

	public TestXmlParserSerializer parse(String xmlContent) {
		document = xps.parseString(xmlContent);
		return this;
	}
	
	public TestXmlParserSerializer withHeader(String xmlHeader) {
		this.xmlHeader = xmlHeader;
		return this;
	}

	public String serialize() {
		String serializedXml = xps.serializeToString(document);
		if (xmlHeader != null) {
			serializedXml = new StringBuffer().append(xmlHeader).append(System.lineSeparator()).append(serializedXml).toString();
		}
		return serializedXml;
	}
}
